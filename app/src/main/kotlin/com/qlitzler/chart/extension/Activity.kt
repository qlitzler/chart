package com.qlitzler.chart.extension

import android.support.v7.app.AppCompatActivity
import com.qlitzler.chart.Chart


inline val AppCompatActivity.chart: Chart get() = application as Chart