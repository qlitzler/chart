package com.qlitzler.chart.extension

import com.qlitzler.chart.Chart
import component.Component


inline val Component.Connection.chart: Chart get() = activity.chart