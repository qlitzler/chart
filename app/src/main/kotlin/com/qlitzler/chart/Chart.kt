package com.qlitzler.chart

import android.app.Application
import com.google.gson.Gson
import com.squareup.leakcanary.LeakCanary
import fixer.FixerRepository
import model.ModelCurrency
import sqlite.SqliteRepository


class Chart : Application() {

    private val gson = Gson()
    lateinit var sqlite: SqliteRepository
    lateinit var fixer: FixerRepository

    override fun onCreate() {
        super.onCreate()

        if (LeakCanary.isInAnalyzerProcess(this)) {
            return
        }

        sqlite = SqliteRepository(baseContext, gson, ModelCurrency.GBP)
        fixer = FixerRepository(baseContext, BuildConfig.DEBUG)

        LeakCanary.install(this)
    }
}