package com.qlitzler.chart.activity

import android.os.Bundle
import com.qlitzler.chart.ChartJob
import com.qlitzler.chart.R
import com.qlitzler.chart.connection.ConnectionDownload
import com.qlitzler.chart.extension.chart
import component.download.DownloadComponent
import component.extension.inflate
import kotlinx.android.synthetic.main.activity.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.run
import org.jetbrains.anko.startActivity


class ActivityDownload : ActivityChart() {

    private val job = ChartJob(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity)

        job += launch {
            if (chart.sqlite.isDatabaseCreated()) {
                startActivity<ActivityHome>()
                finish()
            } else {
                run(UI) {
                    components += DownloadComponent(
                        activity = this@ActivityDownload,
                        bundle = savedInstanceState,
                        view = container.inflate(R.layout.download, true),
                        connection = ConnectionDownload(this@ActivityDownload)
                    )
                }
            }
        }
    }
}