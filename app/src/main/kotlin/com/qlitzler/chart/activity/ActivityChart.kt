package com.qlitzler.chart.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import component.Component


abstract class ActivityChart : AppCompatActivity() {

    protected val components = mutableListOf<Component>()

    protected inline fun <reified T> find(): T? {
        return components.find { it is T } as T?
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        components.forEach { it.save(outState) }
    }
}