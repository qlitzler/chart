package com.qlitzler.chart.activity

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.qlitzler.chart.R
import com.qlitzler.chart.connection.ConnectionCurrency
import com.qlitzler.chart.connection.ConnectionFeatureCurrencyFixer
import com.qlitzler.chart.connection.ConnectionFeatureCurrencySqlite
import com.qlitzler.chart.connection.ConnectionGraph
import com.qlitzler.chart.extension.chart
import component.currency.CurrencyComponent
import component.extension.inflate
import component.graph.GraphComponent
import kotlinx.android.synthetic.main.activity_home.*
import org.jetbrains.anko.startActivity


class ActivityHome : ActivityChart() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        setSupportActionBar(homeToolbar)

        val featureCurrencySqlite = ConnectionFeatureCurrencySqlite(this)
        val featureCurrencyFixer = ConnectionFeatureCurrencyFixer(this)

        components += CurrencyComponent(
            activity = this,
            bundle = savedInstanceState,
            view = homeContainerTop.inflate(R.layout.currency, true),
            connection = ConnectionCurrency(
                activity = this,
                featureCurrencyLocal = featureCurrencySqlite,
                featureCurrencyRemote = featureCurrencyFixer
            )
        )

        components += GraphComponent(
            activity = this,
            bundle = savedInstanceState,
            view = homeContainerBottom.inflate(R.layout.graph, true),
            connection = ConnectionGraph(
                activity = this,
                featureCurrencyLocal = featureCurrencySqlite
            )
        )
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.home, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menuHomeDeleteDatabase -> {
                chart.sqlite.deleteDatabase()
                startActivity<ActivityDownload>()
                finish()
                true
            }
            else -> false
        }
    }
}