package com.qlitzler.chart

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.support.v7.app.AppCompatActivity
import component.extension.plusAssign
import kotlinx.coroutines.experimental.Job


class ChartJob(private val activity: AppCompatActivity) : LifecycleObserver {

    private val jobs = mutableListOf<Job>()

    init {
        activity += this
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun destroy() {
        jobs.forEach { it.cancel() }
        jobs.clear()
    }

    operator fun plusAssign(job: Job) {
        this.jobs += job
    }
}