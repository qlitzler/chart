package com.qlitzler.chart.connection

import android.support.v7.app.AppCompatActivity
import business.chartSimpleDateFormat
import business.convertBase
import business.getMillisSecondsPassedFromTime
import business.toSeconds
import com.qlitzler.chart.ChartJob
import com.qlitzler.chart.extension.chart
import component.extension.plusAssign
import component.graph.GraphComponent
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.run
import model.CurrencyRates
import model.ModelCurrency
import java.util.*
import java.util.concurrent.TimeUnit

class ConnectionGraph(
    override val activity: AppCompatActivity,
    override val featureCurrencyLocal: ConnectionFeatureCurrencySqlite
) : GraphComponent.Connection {

    private val job = ChartJob(activity)

    private val calendar = Calendar.getInstance().also {
        it.set(Calendar.HOUR_OF_DAY, 0)
        it.set(Calendar.MINUTE, 0)
        it.set(Calendar.SECOND, 0)
        it.set(Calendar.MILLISECOND, 0)
    }

    init {
        activity += this
    }

    override fun getTimerAndCurrencyRates(timer: (String) -> Unit, currencyRates: (CurrencyRates) -> Unit) {
        job += launch {
            while (true) {
                val timePassedInMillis = calendar.getMillisSecondsPassedFromTime()
                val currentTimeFormatted = chartSimpleDateFormat.format(Date(timePassedInMillis))
                val timePassedInSeconds = timePassedInMillis.toSeconds().toInt()

                val rates = chart.sqlite.getRates(timePassedInSeconds)
                val convertedCurrencyRates = rates?.rates?.let {
                    val selectedCurrency = featureCurrencyLocal.selectedCurrency.value ?: ModelCurrency.GBP
                    it.convertBase(selectedCurrency)
                }

                run(UI) {
                    timer(currentTimeFormatted)
                    convertedCurrencyRates?.let(currencyRates)
                }
                delay(1, TimeUnit.SECONDS)
            }
        }
    }

    override fun updateRates(rates: CurrencyRates) {
        featureCurrencyLocal.ratesSqlite.postValue(rates)
    }
}


