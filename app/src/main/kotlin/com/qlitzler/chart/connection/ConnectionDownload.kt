package com.qlitzler.chart.connection

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.OnLifecycleEvent
import android.support.v7.app.AppCompatActivity
import com.qlitzler.chart.R
import com.qlitzler.chart.activity.ActivityHome
import com.qlitzler.chart.extension.chart
import component.download.DownloadComponent
import component.extension.plusAssign
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.run
import model.ResponseSuccess
import org.jetbrains.anko.startActivity


class ConnectionDownload(override val activity: AppCompatActivity) : DownloadComponent.Connection {

    private var jobRemote: Job? = null
    private var jobLocal: Job? = null
    private val databaseUrl = activity.getString(R.string.database_url)

    init {
        activity += this
    }

    override fun downloadDatabaseRemote(success: (Boolean) -> Unit) {
        if (jobRemote == null) {
            jobRemote = launch {
                val response = chart.fixer.downloadFile(databaseUrl)

                val boolean = when (response) {
                    is ResponseSuccess -> {
                        try {
                            chart.sqlite.createDatabase(response.data)
                            true
                        } catch (exception: Exception) {
                            exception.printStackTrace()
                            chart.sqlite.deleteDatabase()
                            false
                        }
                    }
                    else -> false
                }
                run(UI) { success(boolean) }
                jobRemote = null
            }
        }
    }

    override fun downloadDatabaseLocal(success: (Boolean) -> Unit) {
        if (jobLocal == null) {
            jobLocal = launch {
                val boolean = try {
                    chart.sqlite.createDatabaseFromAsset()
                    true
                } catch (exception: Exception) {
                    exception.printStackTrace()
                    chart.sqlite.deleteDatabase()
                    false
                }
                run(UI) { success(boolean) }
                jobLocal = null
            }
        }
    }

    override fun downloadSuccess() {
        activity.startActivity<ActivityHome>()
        activity.finish()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun destroy() {
        jobRemote?.cancel()
        jobRemote = null
        jobLocal?.cancel()
        jobLocal = null
    }
}