package com.qlitzler.chart.connection

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.support.v7.app.AppCompatActivity
import com.qlitzler.chart.ChartJob
import com.qlitzler.chart.R
import com.qlitzler.chart.extension.chart
import component.Component
import feature.FeatureCurrency
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.run
import model.CurrencyRates
import model.ModelCurrency


class ConnectionFeatureCurrencySqlite(
    override val activity: AppCompatActivity
) : FeatureCurrency, Component.Connection {

    private val job = ChartJob(activity)
    private val formatNoData = activity.getString(R.string.format_currency_no_data)

    val selectedCurrency = MutableLiveData<ModelCurrency>()
    val ratesSqlite = MutableLiveData<CurrencyRates>()

    override fun getRateForCurrency(currency: ModelCurrency, success: (Float) -> Unit, error: (String) -> Unit) {
        ratesSqlite.observe(activity, Observer { rates ->
            if (rates != null) {
                val float = rates[currency]
                if (float != null) success(float) else error(formatNoData)
            } else error(formatNoData)
        })
    }

    override fun selectCurrency(currency: ModelCurrency) {
        selectedCurrency.postValue(currency)
    }

    override fun getSelectedCurrency(success: (ModelCurrency) -> Unit) {
        selectedCurrency.observe(activity, Observer { selectedCurrency ->
            selectedCurrency?.let(success)
        })
    }

    override fun getCurrencies(success: (List<ModelCurrency>) -> Unit) {
        job += launch {
            val currencies = chart.sqlite.getCurrencies()
            run(UI) { success(currencies) }
        }
    }
}