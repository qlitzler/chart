package com.qlitzler.chart.connection

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.support.v7.app.AppCompatActivity
import com.qlitzler.chart.ChartJob
import com.qlitzler.chart.R
import com.qlitzler.chart.extension.chart
import component.Component
import feature.FeatureCurrency
import kotlinx.coroutines.experimental.launch
import model.CurrencyRates
import model.ModelCurrency
import model.ResponseSuccess


class ConnectionFeatureCurrencyFixer(
    override val activity: AppCompatActivity
) : FeatureCurrency, Component.Connection {

    private val job = ChartJob(activity)
    private val ratesFixer = MutableLiveData<CurrencyRates>()
    private val formatNoData = activity.getString(R.string.format_currency_no_data)

    override fun getRateForCurrency(currency: ModelCurrency, success: (Float) -> Unit, error: (String) -> Unit) {
        ratesFixer.observe(activity, Observer { fixer ->
            if (fixer != null) {
                val float = fixer[currency]
                if (float != null) success(float) else error(formatNoData)
            } else error(formatNoData)
        })
    }

    override fun selectCurrency(currency: ModelCurrency) {
        job += launch {
            ratesFixer.postValue(null)
            val response = chart.fixer.getRates(currency)

            when (response) {
                is ResponseSuccess -> ratesFixer.postValue(response.data.rates)
            }
        }
    }

    override fun getSelectedCurrency(success: (ModelCurrency) -> Unit) = Unit
    override fun getCurrencies(success: (List<ModelCurrency>) -> Unit) = Unit
}