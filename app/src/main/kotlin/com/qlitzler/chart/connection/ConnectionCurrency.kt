package com.qlitzler.chart.connection

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.OnLifecycleEvent
import android.support.v7.app.AppCompatActivity
import component.currency.CurrencyComponent
import component.extension.plusAssign
import model.ModelCurrency


class ConnectionCurrency(
    override val activity: AppCompatActivity,
    override val featureCurrencyLocal: ConnectionFeatureCurrencySqlite,
    override val featureCurrencyRemote: ConnectionFeatureCurrencyFixer
) : CurrencyComponent.Connection {

    init {
        activity += this
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun create() {
        featureCurrencyLocal.selectCurrency(ModelCurrency.GBP)
        featureCurrencyRemote.selectCurrency(ModelCurrency.GBP)
    }
}