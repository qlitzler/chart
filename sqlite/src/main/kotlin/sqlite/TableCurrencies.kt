package sqlite


internal enum class TableCurrencies(override val column: String) : Table {
    Id("_id"),
    Code("code")
}