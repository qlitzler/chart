package sqlite


internal enum class Tables(val tableName: String) {
    Currencies("currencies"),
    Rates("rates")
}