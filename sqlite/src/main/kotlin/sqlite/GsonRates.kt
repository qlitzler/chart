package sqlite

import com.google.gson.annotations.SerializedName


internal data class GsonRates(
    @SerializedName("USD") val usd: Float,
    @SerializedName("AUD") val aud: Float,
    @SerializedName("EUR") val eur: Float
)