package sqlite

import com.google.gson.Gson
import model.ModelCurrency
import model.ModelRates


internal data class ParserRates(
    val id: Long,
    val second: Int,
    val rates: String
)

internal data class ParserRatesExtracted(
    val id: Long,
    val second: Int,
    val gsonRates: GsonRates
)

internal fun ParserRates.toParserRatesExtracted(gson: Gson): ParserRatesExtracted {
    return ParserRatesExtracted(
        id = id,
        second = second,
        gsonRates = gson.fromJson(rates, GsonRates::class.java)
    )
}

internal fun ParserRatesExtracted.toModelRate(baseCurrency: ModelCurrency): ModelRates {
    return ModelRates(
        second = second,
        baseCurrency = baseCurrency,
        rates = mapOf(
            baseCurrency to 1.0f,
            ModelCurrency.USD to gsonRates.usd,
            ModelCurrency.AUD to gsonRates.aud,
            ModelCurrency.EUR to gsonRates.eur
        )
    )
}