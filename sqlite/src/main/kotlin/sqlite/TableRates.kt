package sqlite


internal enum class TableRates(override val column: String) : Table {
    Id("_id"),
    Second("second"),
    Rates("rates")
}