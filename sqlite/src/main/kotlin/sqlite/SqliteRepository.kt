package sqlite

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.google.gson.Gson
import model.ModelCurrency
import model.ModelRates
import org.jetbrains.anko.db.rowParser
import org.jetbrains.anko.db.select
import java.io.File
import java.io.InputStream


class SqliteRepository(
    private val context: Context,
    private val gson: Gson,
    private val baseCurrency: ModelCurrency
) {

    private val fileName = context.getString(R.string.database_name)
    private val file = { context: Context -> File(context.filesDir, fileName) }


    private fun database(): SQLiteDatabase? {
        return try {
            val file = file(context)
            SQLiteDatabase.openDatabase(file.path, null, SQLiteDatabase.OPEN_READONLY)
        } catch (exception: Exception) {
            exception.printStackTrace()
            null
        }
    }

    fun createDatabaseFromAsset() {
        try {
            val file = file(context)
            if (!file.exists()) {
                file.writeFromInputStream(context.assets.open(fileName))
                val database = SQLiteDatabase.openOrCreateDatabase(file, null)
                database.close()
            }
        } catch (exception: Exception) {
            exception.printStackTrace()
        }
    }

    fun deleteDatabase() {
        try {
            file(context).delete()
        } catch (exception: Exception) {
            exception.printStackTrace()
        }
    }

    fun createDatabase(inputStream: InputStream) {
        try {
            val file = file(context)
            file.writeFromInputStream(inputStream)
            val database = SQLiteDatabase.openOrCreateDatabase(file, null)
            database.close()
        } catch (exception: Exception) {
            exception.printStackTrace()
        }
    }

    fun isDatabaseCreated(): Boolean {
        return try {
            file(context).exists()
        } catch (exception: Exception) {
            exception.printStackTrace()
            false
        }
    }

    fun getCurrencies(): List<ModelCurrency> {
        return try {
            database()?.use {
                it.select(Tables.Currencies.tableName)
                    .parseList(rowParser(::ParserCurrencies))
                    .toModelCurrencies()
            } ?: listOf()
        } catch (exception: Exception) {
            listOf()
        }
    }

    fun getRates(second: Int): ModelRates? {
        return try {
            val rates = database()?.use {
                it.select(Tables.Rates.tableName)
                    .whereArgs("${TableRates.Second.name} = $second")
                    .parseOpt(rowParser(::ParserRates))
            }
            return rates?.toParserRatesExtracted(gson)?.toModelRate(baseCurrency)
        } catch (exception: Exception) {
            null
        }
    }

    private fun File.writeFromInputStream(inputStream: InputStream) {
        try {
            val outputStream = outputStream()
            inputStream.copyTo(outputStream)
            outputStream.close()
            inputStream.close()
        } catch (exception: Exception) {
            exception.printStackTrace()
        }
    }
}