package sqlite

import model.ModelCurrency


internal data class ParserCurrencies(
    val id: Long,
    val code: String
)

internal fun List<ParserCurrencies>.toModelCurrencies(): List<ModelCurrency> {
    return map { it.toModelCurrency() }
}

internal fun ParserCurrencies.toModelCurrency(): ModelCurrency {
    return ModelCurrency.valueOf(code)
}