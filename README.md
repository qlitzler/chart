# README #

## App features ##

- Setup remote or local sqlite database
- Ability to select base currency
- Display real time rates from local database
- Display daily rates from fixer.io
- Smooth lines (cubic function)

### Room for improvement: ###

- Reduce overdraw

- Reduce CPU consumption

- Find a way to not use drawPath (CPU intensive)


A simple way to fix CPU performance and remove overdraw is to simply remove 

```
path.lineTo(view.width.toFloat(), view.height.toFloat())
path.lineTo(pointBegin.x, view.height.toFloat())
path.close()
```
 
change `Paint.Style.FILL` to `Paint.Style.STROKE` and add some `strokeWidth`

However, it would not be as cool.



### Things I tried but didn't work out: ###

- Scale the graph with user actions (Impacted performance negatively)

- Draw paths on a bitmap, draw bitmap on canvas (No noticeable improvement)

## Dependencies ##

### Kotlin ###

I've been using Kotlin since early 2016 as my primary programming language for Android development.

### Coroutines ###

Coroutines is a library built on top of Kotlin [suspending functions](https://kotlinlang.org/docs/reference/coroutines.html) feature.

It is an amazing tool to write complex asynchronous code in a synchronous fashion, without ever settings foot in the dreaded Callback Hell.

### Anko ###

Android UI and SQLite helper for Kotlin.

### Android Architecture Lifecycle ###

A great tool to abstract away Android lifecycle using annotations.

This library allows easy implementation of lifecycle aware components, without having to use fragments or god `Activity` classes.

### Android Architecture LiveData ###

Allows for observing data in a lifecycle aware manner.

A simple way to write reactive code without using the cumbersome RxJava library,
while never having to worry about callbacks happening in non-valid lifecycle state.

### Glide, OkHttp, Retrofit, Gson ###

What else ?

## Achitecture ##

This is an architecture I have been using for all of my projects.

It relies on a simple division of responsability on a per module basis.

### Model ###

The *model* module is foundational and stand-alone.

It doesn't know about any other module.

Most of its classes and functions are public.


It should contain:

- Data classes (POJOS) representing business requirements.

- Implementation of business rules (Data classes / Primitive types transformations).

- Unit tests of said Data classes / business rules.

- Description of app features through `Feature` interfaces.


### Data Source ###

A *data source* module encapsulate the logic for accessing a single data source.

This module knows about the *model* module and relevant libraries for the data source.

There can be more than one *data source* module. Example: *api*, *preference*, *sqlite*...

Every classes in these modules should be marked as internal (package private), except for a single entry point.

Internal data classes should be transformed to *model* classes, so they can be exposed to other modules through the entry point.

There shall be no thread management in these modules. Every blocking call should be written in a thread-agnostic fashion.

### Component ###

The *component* module contains UI code, user actions, layouts, styles and themes, encapsulated in classes extending a single and lightweight interface: `Component`.

`Component` classes are lifecycle aware.

This module knows about the *model* module and UI libraries.

As this *module* doesn't know directly about data sources, it relies on interface declarations (`Connection`) to pull / push data.

Components do not know anything about activity navigation.

### App ###

The *app* module is the core package of the app.

It knows about every other module: *model*, *data source*, *component*.

This is where Android `Activity` classes are declared.

Android `Activity` classes are responsible for instantiating and composing `Component` classes, `Connection` implementations and `Feature` implementations.

Thread management can only happen in this module, usually in a `Connection` implementation.

`Connection` implementations can be lifecycle aware to handle threads.

`Connection` implementations should access the relevant data source to retrieve and inject data classes to `Components`.
