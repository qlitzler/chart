package component.download

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.OnLifecycleEvent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import component.Component
import component.extension.plusAssign
import kotlinx.android.synthetic.main.download.view.*


class DownloadComponent(
    override val activity: AppCompatActivity,
    override val bundle: Bundle?,
    override val view: View,
    private val connection: Connection
) : Component {

    interface Connection : Component.Connection {

        fun downloadDatabaseRemote(success: (Boolean) -> Unit)
        fun downloadDatabaseLocal(success: (Boolean) -> Unit)
        fun downloadSuccess()
    }

    private val snackBarError = Snackbar.make(view, "Download failed", Snackbar.LENGTH_LONG)
    private val snackBarProgress = Snackbar.make(view, "Downloading", Snackbar.LENGTH_INDEFINITE)

    init {
        activity += this
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun create() {
        activity.setSupportActionBar(view.downloadToolbar)
        view.downloadButtonRemote.setOnClickListener {
            snackBarProgress.show()
            connection.downloadDatabaseRemote(this::isDownloaded)
        }
        view.downloadButtonLocal.setOnClickListener {
            snackBarProgress.show()
            connection.downloadDatabaseLocal(this::isDownloaded)
        }
    }

    private fun isDownloaded(boolean: Boolean) {
        snackBarProgress.dismiss()
        if (boolean) {
            connection.downloadSuccess()
        } else {
            snackBarError.show()
        }
    }
}