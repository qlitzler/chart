package component.sprite

import android.animation.Animator
import android.animation.ValueAnimator
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.graphics.PointF
import android.view.animation.LinearInterpolator
import component.extension.onEnd
import component.graph.GraphView


class SpriteLine(
    private val view: GraphView,
    private val color: Int,
    val points: MutableList<PointF>,
    val pointEnd: PointF
) : Sprite {

    private val paint: Paint = Paint().also {
        it.color = color
        it.style = Paint.Style.FILL
        it.isAntiAlias = true
    }
    private val path = Path()
    private val lineSmoothness = 0.16f
    private val pointBegin = PointF()

    fun animateEnd(y: Float): Animator {
        return ValueAnimator.ofFloat(pointEnd.y, y).also {
            it.duration = 1000L
            it.interpolator = LinearInterpolator()
            it.addUpdateListener {
                pointEnd.y = it.animatedValue as Float
            }
            it.onEnd { points.add(PointF(view.width.toFloat(), y)) }
            it.start()
        }
    }

    fun removeUnusedPoints() {
        val iterator = points.iterator()
        while (iterator.hasNext()) {
            val point = iterator.next()
            if (point.x <= -view.stepX * 2f) {
                iterator.remove()
            }
        }
    }

    private fun drawCubic(index: Int) {
        val point2 = points[index]
        val point1 = if (index > 0) points[index - 1] else point2
        val point0 = if (index > 1) points[index - 2] else point1
        val point3 = if (index + 1 < points.size) points[index + 1] else point2

        val x1 = point1.x + lineSmoothness * (point2.x - point0.x)
        val y1 = point1.y + lineSmoothness * (point2.y - point0.y)
        val x2 = point2.x - lineSmoothness * (point3.x - point1.x)
        val y2 = point2.y - lineSmoothness * (point3.y - point1.y)

        path.cubicTo(x1, y1, x2, y2, point2.x, point2.y)
    }

    override fun draw(canvas: Canvas) {
        points.forEachIndexed { index, pointF ->
            if (index == 0) {
                pointBegin.set(pointF)
                path.moveTo(pointF.x, pointF.y)
            } else {
                drawCubic(index)
            }
        }
        path.lineTo(pointEnd.x, pointEnd.y)
        path.lineTo(view.width.toFloat(), view.height.toFloat())
        path.lineTo(pointBegin.x, view.height.toFloat())
        path.close()
        canvas.drawPath(path, paint)
        path.reset()
    }
}