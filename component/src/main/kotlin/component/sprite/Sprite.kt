package component.sprite

import android.graphics.Canvas


interface Sprite {

    fun draw(canvas: Canvas)
}