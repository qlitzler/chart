package component.extension

import android.arch.lifecycle.LifecycleObserver
import android.support.v7.app.AppCompatActivity


operator fun AppCompatActivity.plusAssign(observer: LifecycleObserver) {
    lifecycle.addObserver(observer)
}