package component.extension

import android.view.View
import android.view.ViewGroup
import org.jetbrains.anko.layoutInflater


fun ViewGroup.inflate(layoutId: Int, attachToRoot: Boolean = false): View {
    return context.layoutInflater.inflate(layoutId, this, attachToRoot)
}