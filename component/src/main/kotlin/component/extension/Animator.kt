package component.extension

import android.animation.Animator


fun Animator.addListener(
    start: ((Animator) -> Unit)? = null,
    end: ((Animator) -> Unit)? = null,
    repeat: ((Animator) -> Unit)? = null,
    cancel: ((Animator) -> Unit)? = null
): Animator {
    this.addListener(object : Animator.AnimatorListener {
        override fun onAnimationStart(animation: Animator) {
            start?.invoke(animation)
        }

        override fun onAnimationEnd(animation: Animator) {
            end?.invoke(animation)
        }

        override fun onAnimationRepeat(animation: Animator) {
            repeat?.invoke(animation)
        }

        override fun onAnimationCancel(animation: Animator) {
            cancel?.invoke(animation)
        }
    })
    return this
}

fun Animator.onEnd(f: (Animator) -> Unit): Animator {
    return addListener(end = { animator -> f(animator) })
}

fun Animator.onStart(f: (Animator) -> Unit): Animator {
    return addListener(start = { animator -> f(animator) })
}

fun Animator.onRepeat(f: (Animator) -> Unit): Animator {
    return addListener(repeat = { animator -> f(animator) })
}

fun Animator.onCancel(f: (Animator) -> Unit): Animator {
    return addListener(cancel = { animator -> f(animator) })
}

fun Animator.starts(): Animator {
    return also { it.start() }
}