package component

import android.animation.Animator
import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.support.v7.app.AppCompatActivity
import component.extension.plusAssign


class ComponentAnimator(private val activity: AppCompatActivity) : LifecycleObserver {

    private val animators = mutableListOf<Animator>()

    init {
        activity += this
    }

    fun start() {
        animators.forEach(Animator::start)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun destroy() {
        animators.forEach(Animator::cancel)
        animators.clear()
    }

    operator fun plusAssign(animators: Array<out Animator>) {
        this.animators += animators
    }

    operator fun plusAssign(animator: Animator) {
        this.animators += animator
    }
}