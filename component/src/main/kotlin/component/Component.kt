package component

import android.arch.lifecycle.LifecycleObserver
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View


interface Component : LifecycleObserver {

    interface Connection : LifecycleObserver {

        val activity: AppCompatActivity
    }

    val bundle: Bundle?
    val activity: AppCompatActivity
    val view: View

    fun save(bundle: Bundle) = Unit
}