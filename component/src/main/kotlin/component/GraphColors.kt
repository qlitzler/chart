package component

import model.ModelCurrency


val graphColors = mapOf(
    ModelCurrency.EUR.name to R.color.blue,
    ModelCurrency.GBP.name to R.color.green,
    ModelCurrency.USD.name to R.color.pink,
    ModelCurrency.AUD.name to R.color.chestnut
)