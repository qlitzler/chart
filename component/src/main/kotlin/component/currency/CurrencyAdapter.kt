package component.currency

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import component.R
import component.extension.inflate
import component.graph.GraphComponent
import model.ModelCurrency


class CurrencyAdapter(
    private val activity: AppCompatActivity,
    private val bundle: Bundle?,
    private val connection: CurrencyComponent.Connection,
    var items: List<ModelCurrency> = listOf()
) : RecyclerView.Adapter<CurrencyViewHolder>() {

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        return CurrencyViewHolder(
            activity = activity,
            view = parent.inflate(R.layout.currency_item),
            bundle = bundle,
            connection = connection
        )
    }

    override fun getItemCount(): Int = items.size
}
