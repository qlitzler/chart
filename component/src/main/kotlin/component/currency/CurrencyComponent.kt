package component.currency

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.OnLifecycleEvent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import component.Component
import component.extension.plusAssign
import feature.FeatureCurrency
import kotlinx.android.synthetic.main.currency.view.*


class CurrencyComponent(
    override val activity: AppCompatActivity,
    override val bundle: Bundle?,
    override val view: View,
    private val connection: Connection
) : Component {

    interface Connection : Component.Connection {

        val featureCurrencyRemote: FeatureCurrency
        val featureCurrencyLocal: FeatureCurrency
    }

    private val adapter = CurrencyAdapter(activity, bundle, connection)

    init {
        activity += this
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun create() {
        view.currencyList.let {
            it.adapter = adapter
            it.layoutManager = LinearLayoutManager(activity)
        }
        connection.featureCurrencyLocal.getCurrencies { currencies ->
            adapter.items = currencies
            adapter.notifyDataSetChanged()
        }
    }
}