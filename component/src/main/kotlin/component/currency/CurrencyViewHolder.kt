package component.currency

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.View
import component.Component
import component.R
import component.graphColors
import kotlinx.android.synthetic.main.currency_item.view.*
import model.ModelCurrency
import org.jetbrains.anko.sdk25.listeners.onClick
import org.jetbrains.anko.textColor


class CurrencyViewHolder(
    override val activity: AppCompatActivity,
    override val bundle: Bundle?,
    override val view: View,
    private val connection: CurrencyComponent.Connection
) : RecyclerView.ViewHolder(view), Component {

    private val formatCurrency = activity.getString(R.string.format_currency)

    fun bind(currency: ModelCurrency) {
        view.currencyItemName.let {
            it.text = currency.name
            it.textColor = ContextCompat.getColor(activity, graphColors[currency.name] ?: R.color.red)
        }
        connection.featureCurrencyLocal.getRateForCurrency(
            currency,
            success = { rate -> view.currencyItemValue.text = formatCurrency.format(rate) },
            error = { rate -> view.currencyItemValue.text = rate }
        )
        connection.featureCurrencyLocal.getSelectedCurrency { selected ->
            val visibility = if (selected == currency) View.VISIBLE else View.GONE
            view.currencyItemSelected.visibility = visibility
        }
        connection.featureCurrencyRemote.getRateForCurrency(
            currency,
            success = { rate -> view.currencyItemFixerValue.text = formatCurrency.format(rate) },
            error = { rate -> view.currencyItemFixerValue.text = rate }
        )
        view.onClick {
            if (view.currencyItemSelected.visibility == View.GONE) {
                connection.featureCurrencyLocal.selectCurrency(currency)
                connection.featureCurrencyRemote.selectCurrency(currency)
            }
        }
    }
}