package component.graph

import android.animation.Animator
import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.OnLifecycleEvent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import component.Component
import component.ComponentAnimator
import component.R
import component.extension.plusAssign
import feature.FeatureCurrency
import kotlinx.android.synthetic.main.graph.view.*
import model.CurrencyRates


class GraphComponent(
    override val activity: AppCompatActivity,
    override val bundle: Bundle?,
    override val view: View,
    val connection: Connection
) : Component {

    interface Connection : Component.Connection {

        val featureCurrencyLocal: FeatureCurrency

        fun getTimerAndCurrencyRates(timer: (String) -> Unit, currencyRates: (CurrencyRates) -> Unit)
        fun updateRates(rates: CurrencyRates)
    }

    private val animator = ComponentAnimator(activity)
    private var animatorEnd: Animator? = null

    init {
        activity += this
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun create() {
        view.graphScaleY.text = activity.getString(R.string.scale_y).format(view.graphView.currentYCount)
        animator += view.graphView.animateSprites()
        connection.getTimerAndCurrencyRates(
            timer = { timer -> view.graphTimer.text = timer },
            currencyRates = { rates ->
                rates.entries.forEach { (key, value) ->
                    animatorEnd = view.graphView.addNewPoint(key.name, value)
                }
                connection.updateRates(rates)
            }
        )
        connection.featureCurrencyLocal.getSelectedCurrency { view.graphView.restart() }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun destroy() {
        animatorEnd?.cancel()
        animatorEnd = null
    }
}