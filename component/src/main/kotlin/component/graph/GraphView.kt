package component.graph

import android.animation.Animator
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.PointF
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.view.View
import android.view.animation.LinearInterpolator
import component.R
import component.graphColors
import component.sprite.SpriteLine


class GraphView : View {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private val spriteLines = mutableMapOf<String, SpriteLine>()
    private var sprites = listOf<SpriteLine>()

    private var xCount = 60f
    private var yCount = 3.5f

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        sprites.forEach { it.draw(canvas) }
    }

    val currentYCount get() = yCount

    val stepX get() = width / xCount

    val stepY get() = height / yCount

    private fun normalizeY(y: Float) = height - y * stepY

    fun animateSprites(): Animator {
        return ValueAnimator.ofFloat(0f, 1f).also {
            it.duration = 1000
            it.repeatCount = ValueAnimator.INFINITE
            it.repeatMode = ValueAnimator.RESTART
            it.interpolator = LinearInterpolator()
            it.addUpdateListener {
                spriteLines.values.forEach { sprite ->
                    sprite.points.forEach { point ->
                        point.x -= stepX / xCount
                    }
                }
                invalidate()
            }
            it.start()
        }
    }

    fun addNewPoint(key: String, y: Float): Animator {
        val normalizedY = normalizeY(y)
        return spriteLines.getOrPut(key, { makeSprite(key, normalizedY) }).let {
            sprites = spriteLines.values.sortedBy { it.points.sumBy { it.y.toInt() } }
            it.removeUnusedPoints()
            it.animateEnd(normalizedY)
        }
    }

    fun restart() {
        spriteLines.clear()
    }

    private fun makeSprite(key: String, y: Float): SpriteLine {
        return SpriteLine(
            view = this,
            color = ContextCompat.getColor(context, graphColors[key] ?: R.color.red),
            points = mutableListOf(PointF(width.toFloat(), y)),
            pointEnd = PointF(width.toFloat(), y)
        )
    }
}