package business

import model.CurrencyRates
import model.ModelCurrency


fun CurrencyRates.convertBase(base: ModelCurrency): CurrencyRates {
    val baseValue = this[base] ?: 1.0f
    return entries.map { (key, value) ->
        key to value / baseValue
    }.toMap()
}