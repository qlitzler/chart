package business

import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


fun Calendar.getMillisSecondsPassedFromTime(): Long {
    return Calendar.getInstance().timeInMillis - timeInMillis
}

fun Long.toSeconds(): Long {
    return TimeUnit.SECONDS.convert(this, TimeUnit.MILLISECONDS)
}

val chartSimpleDateFormat = SimpleDateFormat("HH:mm:ss", Locale.getDefault()).also { it.timeZone = TimeZone.getTimeZone("UTC") }