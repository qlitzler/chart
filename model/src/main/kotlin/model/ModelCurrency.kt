package model


enum class ModelCurrency {
    GBP,
    EUR,
    USD,
    AUD
}