package model


typealias CurrencyRates = Map<ModelCurrency, Float>
