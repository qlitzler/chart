package model


data class ModelRates(
    val second: Int,
    val baseCurrency: ModelCurrency,
    val rates: Map<ModelCurrency, Float>
)