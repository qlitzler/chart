package feature

import model.ModelCurrency


interface FeatureCurrency {

    fun getRateForCurrency(currency: ModelCurrency, success: (Float) -> Unit, error: (String) -> Unit)
    fun selectCurrency(currency: ModelCurrency)
    fun getSelectedCurrency(success: (ModelCurrency) -> Unit)
    fun getCurrencies(success: (List<ModelCurrency>) -> Unit)
}