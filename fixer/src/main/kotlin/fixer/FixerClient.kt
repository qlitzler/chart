package fixer

import android.content.Context
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


internal class FixerClient(context: Context, private val debug: Boolean) {

    private val timeout = 15L
    private val okHttpInterceptorLogging = HttpLoggingInterceptor().also {
        it.level = if (debug) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
    }

    private val okHttpClient = OkHttpClient.Builder().also {
        it.addInterceptor(okHttpInterceptorLogging)
        it.connectTimeout(timeout, TimeUnit.SECONDS)
        it.readTimeout(timeout, TimeUnit.SECONDS)
        it.writeTimeout(timeout, TimeUnit.SECONDS)
    }.build()

    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(context.getString(R.string.fixer_url))
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    val api: FixerApi = retrofit.create(FixerApi::class.java)
}