package fixer

import com.google.gson.annotations.SerializedName


internal data class FixerLatest(
    @SerializedName("rates") val rates: FixerRates
)