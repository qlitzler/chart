package fixer

import com.google.gson.annotations.SerializedName
import model.ModelCurrency
import model.ModelRates

internal data class FixerRates(
    @SerializedName("GBP") val gpb: Float?,
    @SerializedName("AUD") val aud: Float?,
    @SerializedName("USD") val usd: Float?,
    @SerializedName("EUR") val eur: Float?
)

internal fun FixerRates.toModelRates(base: ModelCurrency): ModelRates {
    val rates = listOf(
        gpb?.let { ModelCurrency.GBP to it },
        eur?.let { ModelCurrency.EUR to it },
        aud?.let { ModelCurrency.AUD to it },
        usd?.let { ModelCurrency.USD to it }
    ).filterNotNull()
    return ModelRates(
        second = 0,
        baseCurrency = base,
        rates = mapOf(
            base to 1.0f,
            *rates.toTypedArray()
        )
    )
}