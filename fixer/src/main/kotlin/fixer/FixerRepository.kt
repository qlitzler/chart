package fixer

import android.content.Context
import model.*
import java.io.InputStream


class FixerRepository(
    context: Context,
    debug: Boolean
) {

    private val client = FixerClient(context, debug)

    fun getRates(base: ModelCurrency): Response<ModelRates> {
        return try {
            val response = client.api.getRates(base.name).execute()

            val body = response.body()
            if (response.isSuccessful && body != null) {
                val data = body.rates.toModelRates(base)
                ResponseSuccess(data)
            } else {
                ResponseError(response.code(), response.errorBody().toString())
            }
        } catch (exception: Exception) {
            ResponseNetworkError(exception)
        }
    }

    fun downloadFile(url: String): Response<InputStream> {
        return try {
            val response = client.api.downloadFile(url).execute()

            val body = response.body()
            if (response.isSuccessful && body != null) {
                ResponseSuccess(body.byteStream())
            } else {
                ResponseError(response.code(), response.errorBody().toString())
            }
        } catch (exception: Exception) {
            ResponseNetworkError(exception)
        }
    }
}